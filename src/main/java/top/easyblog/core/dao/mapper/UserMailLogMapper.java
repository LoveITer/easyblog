package top.easyblog.core.dao.mapper;

import org.springframework.stereotype.Repository;
import top.easyblog.core.dao.mapper.core.BaseMapper;
import top.easyblog.core.dao.model.UserMailLog;


/**
 * @author huangxin
 */
@Repository
public interface UserMailLogMapper extends BaseMapper<UserMailLog> {
    /**
     * @param record
     * @return
     */
    int saveSelective(UserMailLog record);

    /**
     * @param record
     * @return
     */
    int updateByPrimaryKeySelective(UserMailLog record);

    /**
     * @param record
     * @return
     */
    int updateByPrimaryKeyWithContent(UserMailLog record);

}