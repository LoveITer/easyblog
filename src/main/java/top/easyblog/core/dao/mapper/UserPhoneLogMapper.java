package top.easyblog.core.dao.mapper;

import org.springframework.stereotype.Repository;
import top.easyblog.core.dao.mapper.core.BaseMapper;
import top.easyblog.core.dao.model.UserPhoneLog;


/**
 * @author huangxin
 */
@Repository
public interface UserPhoneLogMapper extends BaseMapper<UserPhoneLog> {
    /**
     * @param record
     * @return
     */
    int saveSelective(UserPhoneLog record);

    /**
     * @param record
     * @return
     */
    int updateByPrimaryKeySelective(UserPhoneLog record);

    /**
     * @param record
     * @return
     */
    int updateByPrimaryKeyWithContent(UserPhoneLog record);
}