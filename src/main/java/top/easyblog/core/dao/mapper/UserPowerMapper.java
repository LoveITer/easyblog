package top.easyblog.core.dao.mapper;

import org.springframework.stereotype.Repository;
import top.easyblog.core.dao.mapper.core.BaseMapper;
import top.easyblog.core.dao.model.UserPower;


/**
 * @author huangxin
 */
@Repository
public interface UserPowerMapper extends BaseMapper<UserPower> {
    /**
     * @param record
     * @return
     */
    int saveSelective(UserPower record);

    /**
     * @param record
     * @return
     */
    int updateByPrimaryKeySelective(UserPower record);

}