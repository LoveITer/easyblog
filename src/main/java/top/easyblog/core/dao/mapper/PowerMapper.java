package top.easyblog.core.dao.mapper;

import org.springframework.stereotype.Repository;
import top.easyblog.core.dao.mapper.core.BaseMapper;
import top.easyblog.core.dao.model.Power;


/**
 * @author huangxin
 */
@Repository
public interface PowerMapper extends BaseMapper<Power> {
    /**
     * @param record
     * @return
     */
    int saveSelective(Power record);

    /**
     * @param record
     * @return
     */
    int updateByPrimaryKeySelective(Power record);

}