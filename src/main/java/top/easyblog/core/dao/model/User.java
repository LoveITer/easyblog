package top.easyblog.core.dao.model;



import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Huangxin
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User {

    private Integer userId;
    private String userNickname;
    private String userPassword;
    private String userName;
    private String userGender;
    private Date userBirthday;
    private String userPhone;
    private String userMail;
    private String userAddress;
    private Integer userScore;
    private Integer userRank;
    private String userHeaderImgUrl;
    private String userDescription;
    private Date userRegisterTime;
    private String userRegisterIp;
    private String userLastUpdateTime;
    private Integer userLock;
    private Integer userFreeze;
    private Integer userPower;
    private Integer userLevel;
    private Integer userVisit;
    private String userJobPosition;
    private String userPrefession;
    private String userHobby;
    private String userTech;

}
