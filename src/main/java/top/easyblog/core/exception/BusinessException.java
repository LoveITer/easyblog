package top.easyblog.core.exception;

import top.easyblog.common.response.ResultCode;

/**
 * @author HuangXin
 * @since 2021/8/21 19:03
 */
public class BusinessException extends RuntimeException {


    public ResultCode resultCode;


    public BusinessException(ResultCode resultCode){
        super(resultCode.getCode());
        this.resultCode=resultCode;
    }

}
