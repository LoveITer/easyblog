package top.easyblog.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import top.easyblog.core.dao.mapper.UserAccountMapper;
import top.easyblog.core.dao.model.UserAccount;


import java.util.Objects;

/**
 * @author HuangXin
 * @since 2020/2/5 0:12
 */
@Service
public class UserAccountService {

    @Autowired
    private UserAccountMapper userAccountMapper;



    public boolean isAccountEmpty(int userId) {
        if (userId > 0) {
            try {
                UserAccount account = userAccountMapper.getByUserId(userId);
                return account == null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return true;
    }


    public int createAccount(UserAccount account) {
        if (account != null) {
            try {
                return userAccountMapper.save(account);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return 0;
    }


    public int updateAccountByUserId(UserAccount account) {
        if (Objects.nonNull(account)) {
            try {

                return userAccountMapper.updateSelective(account);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return 0;
    }


    public UserAccount getAccountByUserId(int userId) {
        UserAccount account = null;
        if (userId > 0) {
            try {
                account = userAccountMapper.getByUserId(userId);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            throw new IllegalArgumentException("错误的查询参数：userId=" + userId + "，必须大于0");
        }
        return account;
    }
}
