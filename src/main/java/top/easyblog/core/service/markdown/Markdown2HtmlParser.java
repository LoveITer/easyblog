package top.easyblog.core.service.markdown;

import com.vladsch.flexmark.ext.emoji.EmojiExtension;
import com.vladsch.flexmark.ext.jekyll.tag.JekyllTagExtension;
import com.vladsch.flexmark.ext.tables.TablesExtension;
import com.vladsch.flexmark.ext.toc.SimTocExtension;
import com.vladsch.flexmark.ext.toc.TocExtension;
import com.vladsch.flexmark.html.HtmlRenderer;
import com.vladsch.flexmark.parser.Parser;
import com.vladsch.flexmark.parser.ParserEmulationProfile;
import com.vladsch.flexmark.util.ast.Document;
import com.vladsch.flexmark.util.data.MutableDataSet;
import top.easyblog.core.service.markdown.ext.links.LinkTargetAttributeProvider;
import top.easyblog.core.service.markdown.ext.links.LinkTargetExtensions;
import top.easyblog.core.service.markdown.ext.links.LinkTargetProperties;

import java.util.Arrays;
import java.util.function.Consumer;

/**
 * @author HuangXin
 * @since 2020/1/24 12:00
 * Markdown 渲染为HTML实现
 */
public class Markdown2HtmlParser extends AbstractMarkdownParser {

    @Override
    public String render2Html(String markdown) {
        MutableDataSet options = new MutableDataSet();
        options.setFrom(ParserEmulationProfile.MARKDOWN);
        //扩展功能
        options.set(Parser.EXTENSIONS, Arrays.asList(
                TablesExtension.create(),
                TocExtension.create(),
                JekyllTagExtension.create(),
                SimTocExtension.create(),
                EmojiExtension.create(),
                LinkTargetExtensions.create()
        )).set(TocExtension.LEVELS, 255)
                .set(TocExtension.IS_NUMBERED, false)
                .set(TocExtension.IS_TEXT_ONLY, true)
                .set(TocExtension.TITLE, "目录")
                .set(TocExtension.DIV_CLASS, "toc");
        Parser parser = Parser.builder(options).build();
        HtmlRenderer renderer = HtmlRenderer.builder(options).build();
        Document document = parser.parse(markdown);
        Consumer<Document> accept = (doc) ->
                doc.set(LinkTargetExtensions.LINK_TARGET, new LinkTargetProperties(null, LinkTargetAttributeProvider.TARGET_BLANK, false));
        accept.accept(document);
        return renderer.render(document);
    }

}
