package top.easyblog.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import top.easyblog.core.dao.mapper.UserAttentionMapper;
import top.easyblog.core.dao.model.User;
import top.easyblog.core.dao.model.UserAttention;


import java.util.HashMap;
import java.util.List;
import java.util.Objects;


/**
 * @author huangxin
 */
@Service
public class UserAttentionService {

    @Autowired
    private UserAttentionMapper userAttentionMapper;
    @Autowired
    private  UserService userService;



    public List<UserAttention> getAllUserAttentionInfo(UserAttention userAttention) {
        if (Objects.nonNull(userAttention)) {
            try {
                List<UserAttention> userAttentionInfos = userAttentionMapper.getUserAllAttentionInfoSelective(userAttention);
                if (userAttention.getUserId() != null) {
                    userAttentionInfos.forEach(ele -> {
                        HashMap<String, User> map = new HashMap<>();
                        User user = userService.getUser(ele.getAttentionId());
                        map.put("user", user);
                        ele.setUserInfo(map);
                    });
                } else if (userAttention.getAttentionId() != null) {
                    userAttentionInfos.forEach(ele -> {
                        HashMap<String, User> map = new HashMap<>();
                        User user = userService.getUser(ele.getUserId());
                        map.put("user", user);
                        ele.setUserInfo(map);
                    });
                }
                return userAttentionInfos;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return null;
    }


    public int deleteByPK(int id) {
        if (id > 0) {
            try {
                return userAttentionMapper.deleteByPrimaryKey((long) id);
            } catch (Exception ex) {
                ex.printStackTrace();
                return 0;
            }
        }
        return 0;
    }


    public int countAttentionNumOfMe(int userId) {
        if (userId > 0) {
            try {
                UserAttention userAttention = new UserAttention();
                userAttention.setUserId(userId);
                return userAttentionMapper.countAttentionNumSelective(userAttention);
            } catch (Exception e) {
                return 0;
            }
        }
        return 0;
    }
}
