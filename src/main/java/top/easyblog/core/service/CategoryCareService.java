package top.easyblog.core.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.easyblog.core.dao.mapper.CategoryCareMapper;
import top.easyblog.core.dao.model.CategoryCare;

import java.util.List;


/**
 * @author huangxin
 */
@Service
public class CategoryCareService {

    @Autowired
    private  CategoryCareMapper categoryCareMapper;




    public List<CategoryCare> getCategoryCare(int categoryId) {
        return categoryCareMapper.getCategoryCareByCategoryId(categoryId);
    }


    public int saveCareInfo(int careUserId, int categoryId) {
        CategoryCare categoryCare = new CategoryCare(categoryId, careUserId);
        try {
            return categoryCareMapper.save(categoryCare);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }


    public boolean deleteCareInfo(int userId, int categoryId) {
        try {
            categoryCareMapper.deleteByUserIdAndCategoryId(userId, categoryId);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
