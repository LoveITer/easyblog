package top.easyblog.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import top.easyblog.core.dao.mapper.UserMailLogMapper;
import top.easyblog.core.dao.model.UserMailLog;


/**
 * @author huangxin
 */
@Service
public class UserEmailLogService {

    @Autowired
    private UserMailLogMapper userMailLogMapper;


    public void saveSendCaptchaCode2User(String email, String content) {
        userMailLogMapper.save(new UserMailLog(email, content));
    }


}
