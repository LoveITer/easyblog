package top.easyblog.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import top.easyblog.core.dao.mapper.UserPhoneLogMapper;
import top.easyblog.core.dao.model.UserPhoneLog;


/**
 * @author huangxin
 */
@Service
public class UserPhoneLogService {

    @Autowired
    private UserPhoneLogMapper userPhoneLogMapper;


    public void saveSendCaptchaCode2User(String phone, String content) {
        userPhoneLogMapper.save(new UserPhoneLog(phone, content));
    }
}
