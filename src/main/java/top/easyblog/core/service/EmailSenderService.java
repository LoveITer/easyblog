package top.easyblog.core.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;
import top.easyblog.common.bean.EmailBean;

/**
 * @author huangxin
 */
@Slf4j
@Component
public class EmailSenderService {

    @Autowired
    private JavaMailSender mailSender;


    public boolean send(EmailBean email) {
        try {
            SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
            simpleMailMessage.setSubject(email.getSubject());
            simpleMailMessage.setFrom(email.getSendForm());
            simpleMailMessage.setTo(email.getSendTo());
            simpleMailMessage.setText(email.getSendText());
            mailSender.send(simpleMailMessage);
            return true;
        } catch (Exception e) {
            log.error("发送邮件失败,Exception info:" + e.getMessage());
        }
        return false;
    }

}
