package top.easyblog.core.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.easyblog.common.bean.PageParamBean;
import top.easyblog.common.response.EasyBlogResultCode;
import top.easyblog.common.util.DefaultImageDispatcherUtils;
import top.easyblog.core.dao.mapper.CategoryMapper;
import top.easyblog.core.dao.model.Category;
import top.easyblog.core.exception.BusinessException;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author huangxin
 */
@Service
public class CategoryService {

    @Autowired
    private CategoryMapper categoryMapper;



    public List<Category> getUserAllViableCategory(int userId) {
        List<Category> categories = categoryMapper.getSelective(userId);
        categories.forEach(ele -> {
            if (ele.getCategoryImageUrl() == null || "".equals(ele.getCategoryImageUrl())) {
                ele.setCategoryImageUrl(DefaultImageDispatcherUtils.defaultCategoryImage());
            }
        });
        return categories;
    }


    public Category getCategory(int categoryId) {
        if (categoryId > 0) {
            return categoryMapper.getByPrimaryKey((long) categoryId);
        }
        return null;
    }


    public Category getCategoryByUserIdAndName(int userId, String categoryName) {
        if (userId > 0 && !"".equals(categoryName)) {
            try {
                return categoryMapper.getCategoryByUserIdAndName(userId, categoryName);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return null;
    }


    public int saveCategory(int userId, String categoryName) {
        if (userId > 0 && categoryName != null) {
            Category category = new Category(userId, categoryName, DefaultImageDispatcherUtils.defaultCategoryImage(), 0, 0, 0, "1", "");
            return categoryMapper.insertSelective(category);
        }
        return -1;
    }



    public int saveCategory(Category category) {
        if (Objects.nonNull(category)) {
            try {
                return categoryMapper.save(category);
            } catch (Exception e) {
                return 0;
            }
        }
        return 0;
    }



    public List<Category> getUserAllCategories(int userId) {
        if (userId > 0) {
            try {
                return categoryMapper.getUserAllCategory(userId);
            } catch (Exception e) {
                return null;
            }
        }
        return null;
    }


    public PageInfo<Category> getUserAllCategoriesPage(int userId, PageParamBean pageParamBean) {
        PageInfo<Category> pageInfo = null;
        if (userId > 0) {
            if (Objects.nonNull(pageParamBean)) {
                try {
                    PageHelper.startPage(pageParamBean.getPage(), pageParamBean.getPageSize());
                    List<Category> categories = categoryMapper.getUserAllCategory(userId);
                    pageInfo = new PageInfo<>(categories);
                } catch (Exception e) {
                    throw new RuntimeException(e.getCause());
                }
            } else {
                throw new BusinessException(EasyBlogResultCode.INVALID_PAGE_PARAMS);
            }
        }
        return pageInfo;
    }


    public List<Category> getUserAllDeletedCategory(int userId) {
        if (userId > 0) {
            try {
                return categoryMapper.getUserAllDeletedCategory(userId);
            } catch (Exception ex) {
                ex.printStackTrace();
                return null;
            }
        }
        return null;
    }


    public PageInfo<Category> getUserAllDeletedCategoryPage(int userId, PageParamBean pageParamBean) {
        PageInfo<Category> pageInfo = null;
        if (userId > 0) {
            if (Objects.nonNull(pageParamBean)) {
                try {
                    PageHelper.startPage(pageParamBean.getPage(), pageParamBean.getPageSize());
                    List<Category> categories = categoryMapper.getUserAllDeletedCategory(userId);
                    pageInfo = new PageInfo<>(categories);
                } catch (Exception e) {
                    throw new RuntimeException(e.getCause());
                }
            } else {
                throw new BusinessException(EasyBlogResultCode.INVALID_PAGE_PARAMS);
            }
        }
        return pageInfo;
    }


    public int updateByPKSelective(Category category) {
        if (Objects.nonNull(category)) {
            try {
                return categoryMapper.updateByPrimaryKeySelective(category);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return 0;
    }


    public int updateCategoryInfo(int categoryId, Map<String, Object> params) {
        if (categoryId < 0 || params == null) {
            return 0;
        }
        Category category = new Category();
        category.setCategoryId(categoryId);
        params.forEach((k, v) -> {
            if ("categoryName".equals(k)) {
                category.setCategoryName((String) v);
            }
            if ("categoryImageUrl".equals(k)) {
                category.setCategoryImageUrl((String) v);
            }
            if ("categoryArticleNum".equals(k)) {
                category.setCategoryArticleNum((Integer) v);
            }
            if ("categoryClickNum".equals(k)) {
                category.setCategoryClickNum((Integer) v);
            }
            if ("categoryCareNum".equals(k)) {
                category.setCategoryCareNum((Integer) v);
            }
            if ("display".equals(k)) {
                category.setDisplay(v + "");
            }
        });
        try {
            return categoryMapper.updateByPrimaryKeySelective(category);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }


    public int deleteCategoryByCondition(Category category) {
        if (Objects.nonNull(category)) {
            try {
                return categoryMapper.deleteSelective(category);
            } catch (Exception e) {
                return 0;
            }
        }
        return 0;
    }


    public int countSelective(Category category) {
        if (Objects.nonNull(category)) {
            try {
                return categoryMapper.countSelective(category);
            } catch (Exception e) {
                e.printStackTrace();
                return 0;
            }
        }
        return 0;
    }
}
