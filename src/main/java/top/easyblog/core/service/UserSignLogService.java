package top.easyblog.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import top.easyblog.core.dao.mapper.UserSigninLogMapper;
import top.easyblog.core.dao.model.UserSigninLog;

import java.util.List;


/**
 * @author huangxin
 */
@Service
public class UserSignLogService {

    @Autowired
    private UserSigninLogMapper userSigninLogMapper;


    public int saveSigninLog(UserSigninLog log) {
        try {
            return userSigninLogMapper.save(log);
        } catch (Exception e) {
            e.getMessage();
            return -1;
        }
    }



    public List<UserSigninLog> getUserLoginInfo(int userId, int num) {
        if (userId > 0) {
            try {
                return userSigninLogMapper.getUserLoginInfo(userId, num);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return null;
    }
}
