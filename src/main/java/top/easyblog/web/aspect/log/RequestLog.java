package top.easyblog.web.aspect.log;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Arrays;

/**
 * @author ：huangxin
 * @modified ：
 * @since ：2020/10/12 17:49
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RequestLog  {

    private String url;
    private String ipInfo;
    private String classMethod;
    private Object[] args;


    @Override
    public String toString() {
        return String.format("[request] => %s , [client ip] => %s , [invoke method] => %s , [params] => %s",url,ipInfo,classMethod, Arrays.toString(args));
    }

}
