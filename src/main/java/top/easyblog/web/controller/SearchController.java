package top.easyblog.web.controller;

import com.github.pagehelper.PageInfo;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import top.easyblog.common.util.CookieUtils;
import top.easyblog.common.util.SensitiveWordUtils;
import top.easyblog.common.util.UserUtils;
import top.easyblog.core.dao.model.Article;
import top.easyblog.core.dao.model.User;
import top.easyblog.common.bean.PageParamBean;
import top.easyblog.common.enums.PageSizeEnum;


import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author huangxin
 */
@RequestMapping(value = "/search")
@Controller
public class SearchController extends BaseController {


    private static final String PLACEHOLDER = "请输入关键字词";


    /**
     * 搜索结果显示
     *
     * @param query  查询的关键字
     * @param pageNo 分页号
     * @param model  Model
     */
    @GetMapping("/details")
    public String showSearchResult(@RequestParam String query,
                                   @RequestParam(value = "page", defaultValue = "1") int pageNo,
                                   HttpServletRequest request,
                                   Model model) {
        if (query == null || query.replaceAll(" ", "").length() == 0 || PLACEHOLDER.equals(query)) {
            String backward = request.getHeader(HttpHeaders.REFERER);
            return "redirect:" + backward;
        }
        String baseUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
        model.addAttribute("baseUrl", baseUrl);
        String sessionId = CookieUtils.getCookieValue(request, USER_LOGIN_TOKEN);
        User visitor = UserUtils.getUserFromRedis(sessionId);
        model.addAttribute("user", visitor);
        model.addAttribute("query", query);
        SensitiveWordUtils wordUtils = SensitiveWordUtils.getInstance();
        //检测是否有敏感字
        int index = wordUtils.CheckSensitiveWord(query, 0, SensitiveWordUtils.MatchType.MIN_MATCH_TYPE);
        if (index == 0) {
            //如果有敏感字就不往热搜词排行榜总存放
            hotWordService.incrementScore(query);
        }
        //重新获取一下热搜词排行前15的关键字
        List<String> hotList = hotWordService.getHotList(null);
        model.addAttribute("hotList", hotList);
        PageInfo<Article> articlePages = articleService.getArticleByTopicPage(query,  PageParamBean.builder().page(pageNo).pageSize(PageSizeEnum.MIN_PAGE_SIZE).build());
        model.addAttribute("articlePages", articlePages);

        //热文推荐
        List<Article> allHistoryFamousArticles = articleService.getAllHistoryFamousArticles(10);
        model.addAttribute("hotArticles", allHistoryFamousArticles);
        return "search";
    }

}
