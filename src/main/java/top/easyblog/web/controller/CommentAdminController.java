package top.easyblog.web.controller;

import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import top.easyblog.common.bean.PageParamBean;
import top.easyblog.common.constant.HttpConstant;
import top.easyblog.common.enums.PageSizeEnum;
import top.easyblog.common.util.CookieUtils;
import top.easyblog.common.util.UserUtils;
import top.easyblog.core.dao.model.User;
import top.easyblog.core.dao.model.UserComment;
import top.easyblog.core.service.CommentService;
import top.easyblog.web.config.WebAjaxResult;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * @author huangxin
 */
@Controller
@RequestMapping(value = "/manage/comment")
public class CommentAdminController extends BaseController {

    private static final String PREFIX = "/admin/comment_manage/";

    @Autowired
    private CommentService commentService;



    @GetMapping(value = "/publish")
    public String commentPublishListPage(HttpServletRequest request,
                                         Model model,
                                         @RequestParam(value = "page", defaultValue = "1") int pageNo) {
        String sessionId = CookieUtils.getCookieValue(request, USER_LOGIN_TOKEN);
        User user = UserUtils.getUserFromRedis(sessionId);
        if (Objects.nonNull(user)) {
            model.addAttribute("user", user);
            model.addAttribute("visitor", user);
            PageInfo<UserComment> commentsPage = commentService.getCommentPage(user.getUserId(), "send",
                    PageParamBean.builder().page(pageNo).pageSize(PageSizeEnum.MIN_PAGE_SIZE).build());
            model.addAttribute("commentsPage", commentsPage);
            return PREFIX + "comment-manage-publish";
        }
        return HttpConstant.LOGIN_PAGE;
    }


    @GetMapping(value = "/receive")
    public String commentReceiveListPage(Model model,
                                         HttpServletRequest request,
                                         @RequestParam(value = "page", defaultValue = "1") int pageNo) {
        String sessionId = CookieUtils.getCookieValue(request, USER_LOGIN_TOKEN);
        User user = UserUtils.getUserFromRedis(sessionId);
        if (Objects.nonNull(user)) {
            model.addAttribute("user", user);
            model.addAttribute("visitor", user);
            PageInfo<UserComment> commentsPage = commentService.getCommentPage(user.getUserId(), "receive",
                    PageParamBean.builder().page(pageNo).pageSize(PageSizeEnum.MIN_PAGE_SIZE).build());
            model.addAttribute("commentsPage", commentsPage);
            return PREFIX + "comment-manage-receive";
        }
        return HttpConstant.LOGIN_PAGE;
    }


    @ResponseBody
    @GetMapping(value = "/delete")
    public WebAjaxResult deleteComment(HttpServletRequest request,
                                       @RequestParam int commentId) {
        String sessionId = CookieUtils.getCookieValue(request, USER_LOGIN_TOKEN);
        User user = UserUtils.getUserFromRedis(sessionId);
        WebAjaxResult ajaxResult = new WebAjaxResult();
        ajaxResult.setMessage("请登录后再操作！");
        if (Objects.nonNull(user)) {
            int var0 = commentService.deleteComment(commentId);
            if (var0 == 1) {
                ajaxResult.setSuccess(true);
            }
            ajaxResult.setMessage("抱歉！删除失败");
            return ajaxResult;
        }
        return ajaxResult;
    }


}
