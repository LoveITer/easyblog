package top.easyblog.common.response;

/**
 * 响应码
 *
 * @author HuangXin
 * @since 2021/8/21 22:13
 */
public enum EasyBlogResultCode implements ResultCode{
    SUCCESS,
    INVALID_PAGE_PARAMS
    ;

    @Override
    public String getCode(){
        return this.name().toLowerCase();
    }
}
