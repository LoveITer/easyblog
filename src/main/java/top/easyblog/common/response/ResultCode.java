package top.easyblog.common.response;

/**
 * @author HuangXin
 * @since 2021/8/21 22:28
 */
public interface ResultCode {
    String getCode();
}
