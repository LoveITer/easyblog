package top.easyblog.common.constant;

/**
 * @author HuangXin
 * @since 2021/8/22 0:45
 */
public interface HttpConstant {
    /**
     * 登录页面
     */
    String LOGIN_PAGE = "redirect:/user/login.html";
    /**
     * 404页面路径
     */
    String PAGE404 = "redirect:/error/404";
    /**
     * 错误页面
     */
    String ERROR_PAGE = "redirect:/error/error";

    String BLOG_MANAGE_PAGE_PREFIX = "admin/blog_manage";

    /**
     * 用户登录token
     */
    String USER_LOGIN_TOKEN = "user_login_token";
    /**
     * 用户信息标记
     */
    String REMEMBER_ME_COOKIE = "REMEMBER-ME-COOKIE";

    /**
     * 用户登录信息最大保存时间（免登陆最大时间）:60天
     */
    int MAX_USER_LOGIN_STATUS_KEEP_TIME = 60 * 60 * 24 * 60;

    /**
     * 浏览器保存用户名，密码90天
     */
    int REMEMBER_ME_TIME = 60 * 60 * 24 * 90;
}
