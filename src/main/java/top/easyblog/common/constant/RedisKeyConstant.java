package top.easyblog.common.constant;

/**
 * @author HuangXin
 * @since 2021/8/22 0:56
 */
public interface RedisKeyConstant {
    /**
     * 用户登录token
     */
    String USER_LOGIN_TOKEN = "user:login-token:{id}";
}
