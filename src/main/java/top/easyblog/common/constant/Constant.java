package top.easyblog.common.constant;

/**
 * 系统公共常量
 *
 * @author HuangXin
 * @since 2021/8/21 22:13
 */
public interface Constant {
    /**
     * 关于我页面的默认文章显示数
     **/
     int HOME_PAGE_DEFAULT_ARTICLE_SIZE = 15;
    /***默认文章描述长度*/
     int ARTICLE_DESCRIPTION_SIZE = 80;


}
