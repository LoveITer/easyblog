package top.easyblog.common.bean;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import top.easyblog.common.enums.PageSizeEnum;

import java.util.Objects;

/**
 * 分页参数设置
 *
 * @author huangxin
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PageParamBean {

    /**
     * 分页页数
     */
    private Integer page;

    private Integer limit;

    private Integer offset;

    private Integer amount;

    /**
     * 分页大小
     */
    private PageSizeEnum pageSize;


    public void setPageSize(PageSizeEnum pageSize) {
        if (Objects.isNull(pageSize)) {
            //默认每页的大小是15条数据
            this.pageSize = PageSizeEnum.DEFAULT_PAGE_SIZE;
        } else {
            this.pageSize = pageSize;
        }
    }


    public Integer getPageSize(){
        return this.pageSize.getPageSize();
    }

}
