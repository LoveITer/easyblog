package top.easyblog.common.bean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author huangxin
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EmailBean {

    private String subject;
    private String sendForm = "2489868503@qq.com";
    private String sendTo;
    private String sendText;
    private String attachment;

}

