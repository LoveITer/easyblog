package top.easyblog.common.enums;

/**
 * 文章的类型
 *
 * @author huangxin
 */
public enum ArticleType {
    /**
     * 原创文章
     */
    ORIGINAL("0"),
    /**
     * 转载文章
     */
    REPRINT("1"),
    /**
     * 翻译文章
     */
    TRANSLATE("2"),
    /**
     * 不限
     */
    UNLIMITED("3");

    String articleType;

    ArticleType(String articleType) {
        this.articleType = articleType;
    }

    public String getArticleType() {
        return articleType;
    }

    public void setArticleType(String articleType) {
        this.articleType = articleType;
    }
}
